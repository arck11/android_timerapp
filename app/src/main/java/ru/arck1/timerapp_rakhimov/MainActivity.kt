package ru.arck1.timerapp_rakhimov

import android.os.Bundle
import android.os.Handler
import android.os.Message
import android.widget.Button
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import java.lang.ref.WeakReference


class MainActivity : AppCompatActivity() {

    private var seconds: Int = 0
    private var running: Boolean = false

    private val timer_format = "%s : %s : %s"

    private var mHandler: Handler? = null
    private lateinit var mRunnable: Runnable


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val saved_seconds = savedInstanceState?.getInt("seconds", 0)
        val saved_running = savedInstanceState?.getBoolean("running", false)

        if (saved_seconds != null)
            this.seconds = saved_seconds
        if (saved_running != null)
            this.running = saved_running

        val startBtn: Button = findViewById(R.id.start_btn)
        val stopBtn: Button = findViewById(R.id.stop_btn)
        val resetBtn: Button = findViewById(R.id.reset_btn)

        startBtn.setOnClickListener {
            onClickStart()
        }
        stopBtn.setOnClickListener {
            onClickStop()
        }
        resetBtn.setOnClickListener {
            onClickReset()
        }

        mHandler = Handler()

        mRunnable = Runnable {
            this.runTimer()

            mHandler?.postDelayed(
                mRunnable,
                1000
            )
        }

        mHandler?.postDelayed(
            mRunnable,
            1000
        )

    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)

        outState.putInt("seconds", this.seconds)
        outState.putBoolean("running", this.running)
    }

    fun runTimer() {
        if (this.running) {

            this.seconds++

            val tmr: TextView = findViewById(R.id.timer_textview)

            val hour: String = (this.seconds / 3600).toString().padStart(2, '0')
            val minutes: String = ((this.seconds % 3600) / 60).toString().padStart(2, '0')
            val sec = ( this.seconds % 60).toString().padStart(2, '0')

            tmr.text = this.timer_format.format(
                hour, minutes, sec
            )
        }
    }

    fun onClickStart() {
        this.running = true

    }

    fun onClickStop() {
        this.running = false
    }

    fun onClickReset() {
        this.running = false
        this.seconds = 0

        val tmr: TextView = findViewById(R.id.timer_textview)
        tmr.text = "00 : 00 : 00"

    }
}
